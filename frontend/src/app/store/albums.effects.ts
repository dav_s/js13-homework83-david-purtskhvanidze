import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AlbumsService } from '../services/albums.service';
import {
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  fetchAlbumsFailure
} from './albums.actions';
import { mergeMap, map, catchError, of } from 'rxjs';

@Injectable()
export class AlbumsEffects {
  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(() => this.albumsService.getAlbums().pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));


  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
  ) {
    console.log(fetchAlbumsRequest)
  }
}
