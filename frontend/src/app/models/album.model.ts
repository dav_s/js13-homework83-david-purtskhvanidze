export class Album {
  constructor(
    public id: string,
    public title: string,
    public artist: string,
    public release: string,
    public image: string,
  ) {}
}

export interface AlbumData {
  [key: string]: any;
  title: string;
  artist: string,
  release: string;
  image: File | null;
}

export interface ApiAlbumData {
  _id: string,
  title: string;
  artist: string,
  release: string;
  image: string;
}
