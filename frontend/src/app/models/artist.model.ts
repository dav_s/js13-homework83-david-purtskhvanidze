export class Artist {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public image: string,
  ) {}
}

export interface ArtistData {
  [key: string]: any;
  title: string;
  description: string;
  image: File | null;
}

export interface ApiArtistData {
  _id: string,
  title: string,
  description: string,
  image: string
}
