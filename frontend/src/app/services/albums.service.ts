import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Album, ApiAlbumData } from '../models/album.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(
    private http: HttpClient,
  ) { }

  getAlbums() {
    return this.http.get<ApiAlbumData[]>(environment.apiUrl + '/albums').pipe(
      map(response => {
        return response.map(albumData => {
          return new Album(
            albumData._id,
            albumData.title,
            albumData.artist,
            albumData.release,
            albumData.image,
          );
        });
      })
    );
  }
}
