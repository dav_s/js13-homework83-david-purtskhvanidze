import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../../models/album.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchAlbumsRequest } from '../../store/albums.actions';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.sass']
})
export class ArtistComponent implements OnInit {
  albums: Observable<Album[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchAlbumsRequest());
  }

}
