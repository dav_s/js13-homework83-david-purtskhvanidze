const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: String,
    image: String
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;